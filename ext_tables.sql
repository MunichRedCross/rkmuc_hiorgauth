#
# Modify fe_users, fe_groups
#
CREATE TABLE fe_groups (
    tx_rkmuc_hiorg_ov VARCHAR(6) NOT NULL DEFAULT '',
    tx_rkmuc_hiorg_sid int(11) NULL DEFAULT NULL,
    tx_rkmuc_parent_id int(11) NULL DEFAULT NULL,
    tx_rkmuc_addgroups TINYTEXT NULL DEFAULT NULL
);

CREATE TABLE fe_users (
    tx_rkmuc_hiorg_uid VARCHAR(32) NULL DEFAULT NULL
);
