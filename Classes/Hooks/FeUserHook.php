<?php
namespace Rkmuc\Hiorgauth\Hooks;

use TYPO3\CMS\Core\Utility\GeneralUtility;	 
use TYPO3\CMS\Core\Utility\HttpUtility;
use TYPO3\CMS\Core\Service\MarkerBasedTemplateService;
	 
class FeUserHook {
	
	public function postProcContent ( $params, $_this ) {
	
                $logger = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Log\\LogManager')->getLogger(__CLASS__);
                $logger->debug('hook');
		$_this->spid = 0;
                $logger->debug($_this->spid);

		$cObj = GeneralUtility::makeInstance(MarkerBasedTemplateService::class);

		$marker = [];

		$marker['###OV_LABEL###'] = 'Org.:';

                $subTemplate = $cObj->getSubpart($params['content'], '###OV_ITEM###');

                $subMarker = [];
                $subPartContent = "";

                // get groups
                $fields = 'title,tx_rkmuc_hiorg_ov';
                $table = 'fe_groups';
                $where = 'pid IN(' . $_this->spid . ') ';
                $where .= 'AND hidden = 0 AND IFNULL (tx_rkmuc_hiorg_ov,"") != ""';
                $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery($fields, $table, $where);
                $logger->debug($fields);
                $logger->debug($table);
                $logger->debug($where);
                $count = $GLOBALS['TYPO3_DB']->sql_num_rows($res);
                $logger->debug($count);

                while ($data = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
                    $subMarker['###OV_CODE###'] = $data['tx_rkmuc_hiorg_ov'];
                    $subMarker['###OV_NAME###'] = $data['title'];

                    $subPartContent .= $cObj->substituteMarkerArray($subTemplate, $subMarker);
                }

		$content = $cObj->substituteMarkerArray($params['content'], $marker);
                $content = $cObj->substituteSubpart($content, '###OV_ITEM###', $subPartContent);

		return $content;
        }

	public function logoutConfirmed ( $params, $_this ) {
                $logger = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Log\\LogManager')->getLogger(__CLASS__);
                $logger->info('logout_confirmed');

                $authUrl = "https://www.hiorg-server.de/logmein.php";
                $request_uri = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                $path_info = substr($request_uri,0,-strlen($_SERVER['QUERY_STRING'])-2);
                $token = $GLOBALS['TSFE']->fe_user->getKey('ses', 'hiorg_token');
                if (!empty($token)) {
                    $GLOBALS['TSFE']->fe_user->setKey('ses', 'hiorg_token', '');
                    $logoutUrl = $authUrl . "?logout=1&token=" . $token . "&weiter=".urlencode($request_uri);
                    $logger->info($logoutUrl);
                    HttpUtility::redirect($logoutUrl, HttpUtility::HTTP_STATUS_303);
                }


        }
}
