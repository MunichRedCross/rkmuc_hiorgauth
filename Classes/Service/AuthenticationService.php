<?php
namespace Rkmuc\Hiorgauth\Service;

use TYPO3\CMS\Core\Utility\HttpUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\FrontendUser;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Crypto\Random;
//use TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication;

$typo3Branch = class_exists(\TYPO3\CMS\Core\Information\Typo3Version::class)
    ? (new \TYPO3\CMS\Core\Information\Typo3Version())->getBranch()
    : TYPO3_branch;
if (version_compare($typo3Branch, '9.0', '>=')) {
    class BaseAuthenticationService extends \TYPO3\CMS\Core\Authentication\AuthenticationService {}
} else {
    class BaseAuthenticationService extends \TYPO3\CMS\Sv\AuthenticationService {}
}


class AuthenticationService extends BaseAuthenticationService {

    /** @var \TYPO3\CMS\Core\Database\DatabaseConnection */
    protected $databaseConnection;

    /** @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManager */
//    protected $configurationManager;

    /** pidlist felogin */
//    protected $pidList;
    protected $storagePid;

    /**
     * Initializes the class.
     */
    public function __construct() {
            $this->logger = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Log\\LogManager')->getLogger(__CLASS__);
            $this->logger->debug('construct');

            $this->databaseConnection = $GLOBALS['TYPO3_DB'];

            $this->storagePid = 0;

	    $this->context = GeneralUtility::makeInstance(Context::class);

//            $objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\Extbase\\Object\\ObjectManager');
//            $this->configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface');
//            $this->configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManager');

    }

  private $userId;

public function getUser(){
    $authUrl = "https://www.hiorg-server.de/logmein.php";
    if (isset($_REQUEST['redirect_url'])) {
        $request_uri = "https://$_SERVER[HTTP_HOST]" . $_REQUEST['redirect_url'];
    } else {
        $request_uri = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }
    $path_info = substr($request_uri,0,-strlen($_SERVER['QUERY_STRING'])-2);
    $userinfo  = "user_id,username,name,vorname,gruppe,perms";

    $logger = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Log\\LogManager')->getLogger(__CLASS__);
    $this->logger->debug('getUser');
    $this->logger->debug($this->storagePid);
    $this->logger->debug($this->_spid);
    $this->logger->debug($request_uri);
    $this->logger->debug($path_info);
    $this->logger->debug(serialize($_SERVER));


    // Check Organisationskuerzel 
    $ov = $_POST['ov'];
    if (empty($ov)) $ov = $_GET['ov'];

    if (!empty($ov)) {
        // check for group
        $fields = 'uid';
        $table = 'fe_groups';
        $where = 'pid IN(' . $this->storagePid . ') ';
        $where .= "AND hidden = 0 AND tx_rkmuc_hiorg_ov = '" . $ov . "'";
        $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery($fields, $table, $where);
        $count = $GLOBALS['TYPO3_DB']->sql_num_rows($res);
        
        if ($count == 0) {
            unset($ov);
        }
    }


    if(!empty($_GET["token"])) {
        $token = $_GET["token"];
        $this->logger->debug('token');
        $this->logger->debug($token);

        // Check token with curl
        $curl = curl_init($authUrl."?token=".urlencode($token));
        $this->logger->debug($authUrl."?token=".urlencode($token));
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $curl_data = trim(curl_exec($curl));

        $this->logger->debug('curl error');
        $this->logger->debug(curl_error($curl));
        $this->logger->debug('curl data');
        $this->logger->debug($curl_data);

        // unnkown token
        if(mb_substr( $curl_data ,0,2) != "OK") {
            HttpUtility::redirect($path_info, HttpUtility::HTTP_STATUS_303);
            return false;
        }

        // read user data
        $userdata = unserialize(base64_decode(mb_substr( $curl_data , 3)));
        $this->logger->debug(serialize($userdata));

//	$this->checkPid = false;
//	$this->db_user['checkPid'] = false;
//        $GLOBALS['TSFE']->fe_user->checkPid = 0;
/*
        $info = $GLOBALS['TSFE']->fe_user->getAuthInfoArray();
        $this->logger->error('info');
        $this->logger->error(serialize($info['db_user']));
*/
        $username = $userdata['ov'] . ":" . $userdata['username'];
        //$user = $GLOBALS['TSFE']->fe_user->fetchUserRecord($info['db_user'], "", "AND tx_rkmuc_hiorg_uid = '" . $userdata['user_id'] . "'");
        $user = $this->fetchUserRecord(null, "AND tx_rkmuc_hiorg_uid = '" . $userdata['user_id'] . "'");
        //$user = $this->fetchUserRecord($username);
        $this->logger->debug(serialize($user));

        // get main group ids
        $fields = 'uid,tx_rkmuc_addgroups,title';
        $table = 'fe_groups';
        $where = 'pid IN(' . $this->storagePid . ') ';
        $where .= "AND tx_rkmuc_hiorg_ov = '" . $userdata['ov'] . "'";
        $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery($fields, $table, $where);
        $count = $GLOBALS['TYPO3_DB']->sql_num_rows($res);

        $this->logger->debug('feg');

        // No group -> error
        if ($count == 0) {
            HttpUtility::redirect($path_info, HttpUtility::HTTP_STATUS_303);
            return false;
        }

        // build array
        $groups = array();
        $data = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
        $groups[] = intval($data['uid']);
        if (!empty($data['tx_rkmuc_addgroups'])) {
            foreach (explode(',',$data['tx_rkmuc_addgroups']) as $g) {
            $groups[] = intval($g);
            }
        }

        // Leitung
        if (in_array('admin', explode(',',$userdata['perms']))) $groups[] = 7;

        // get ids of subgroups
        $fields = 'uid, tx_rkmuc_hiorg_sid, hidden';
        $table = 'fe_groups';
        $where = 'pid IN(' . $this->storagePid . ') ';
        $where .= "AND tx_rkmuc_parent_id = '" . $data['uid'] . "'";
        $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery($fields, $table, $where);
        $count = $GLOBALS['TYPO3_DB']->sql_num_rows($res);

        // Need to create subgroups first
        if ($count == 0) {
            for ($id = 1; $id <= 1024; $id = $id * 2) {
                $newgrp = array(
                        'pid' => $this->storagePid,
			'crdate' => time(),
			'tstamp' => time(),
                        'title' => $data['title'] . ":" . $id,
                        'description' => 'Untergruppe HiOrg',
                        'tx_rkmuc_hiorg_sid' => intval($id),
                        'tx_rkmuc_parent_id' => intval($data['uid']),
                ); 

                $this->databaseConnection->exec_INSERTquery('fe_groups', $newgrp);
            }

            // Reread groups
            $fields = 'uid, tx_rkmuc_hiorg_sid, hidden';
            $table = 'fe_groups';
            $where = 'pid IN(' . $this->storagePid . ') ';
            $where .= "AND tx_rkmuc_parent_id = '" . $data['uid'] . "'";
            $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery($fields, $table, $where);
            $count = $GLOBALS['TYPO3_DB']->sql_num_rows($res);

        }

        while ($grp = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
            // skip hidden groups
            if ($grp['hidden'] == 1) continue;

            if (0 != ($grp['tx_rkmuc_hiorg_sid'] & $userdata['gruppe']))  {
                $groups[] = intval($grp['uid']);
            }   
        }
        
        // sort array
        $groups = array_unique($groups);
            
        // Create user
        if (empty($user)) {

		$newuser = array(
			'username' => $username,
			'pid' => $this->storagePid,
			'crdate' => time(),
			'tstamp' => time(),
                        'usergroup' => implode(',',$groups),
                        'tx_rkmuc_hiorg_uid' => $userdata['user_id'],
                        'name' => $userdata['vorname'] . " " . $userdata['name'],
                        'password' => GeneralUtility::makeInstance(Random::class)->generateRandomHexString(32),
		);
                $this->databaseConnection->exec_INSERTquery('fe_users', $newuser);
            $this->logger->debug('new user');
            $this->logger->debug(serialize($newuser));
                // Get userdata
//                $user = $GLOBALS['TSFE']->fe_user->fetchUserRecord($info['db_user'], "", "AND tx_rkmuc_hiorg_uid = '" . $userdata['user_id'] . "'");
		$user = $this->fetchUserRecord(null, 'AND tx_rkmuc_hiorg_uid = "' . $userdata['user_id'] . '"');
            $this->logger->debug('user');
            $this->logger->debug(serialize($user));
        
            } else {

		$user['name'] = $userdata['vorname'] . " " . $userdata['name'];
                $user['usergroup'] = implode(',',$groups);
                $user['tx_rkmuc_hiorg_uid'] = $userdata['user_id'];

                $this->databaseConnection->exec_UPDATEquery('fe_users',
                        'uid=' . $this->databaseConnection->fullQuoteStr($user['uid'], 'fe_users'),
                        $user
                    );
            }

            $user['auth'] = 'hiorg';
            $user['hiorg_token'] = $token;

            $this->logger->debug(serialize($user));

            return $user;

     } elseif ($ov) {
        $this->logger->debug('redirect');
        $authorizationUrl = $authUrl . "?ov=$ov&weiter=".urlencode($request_uri)."&getuserinfo=".urlencode($userinfo);
        $this->logger->debug($authorizationUrl);
        HttpUtility::redirect($authorizationUrl, HttpUtility::HTTP_STATUS_303);
     } else {
        // Do nothing
        $this->logger->debug('do nothing');
     }
  }

  public function processLoginData(array &$loginData, $passwordTransmissionStrategy){
    // do nothing...
  }

  public function authUser(array $user) : int {
     // 0: hard fail
     // 100: soft fail
     // 200: success
     $logger = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Log\\LogManager')->getLogger(__CLASS__);
    $this->logger->debug('authUser');
    $this->logger->debug(var_export($user,true));

    if($user['auth'] == 'hiorg') {
        $this->logger->debug('success');
        //$GLOBALS['TSFE']->fe_user->setKey('ses', 'hiorg_token', $user['hiorg_token']);
        //$this->setKey('ses', 'hiorg_token', $user['hiorg_token']);
      // OK, 'code' = 200
      return 200;
    }
    // fail of this auth module
    return 100;
  }

  public function getGroups($user, $knownGroups) {
    // do nothing...
  }

}
