<?php
if (!defined('TYPO3_MODE')) {
  die ('Access denied.');
}
  
// Configure new fields:
$fields = array(
  'tx_rkmuc_hiorg_uid' => array(
    'label' => 'HiOrg User Id',
    'exclude' => 1,
    'config' => array(
      'type' => 'input',
      'readOnly' => 1,
      'max' => 32,
    ),
  ),
);
  
// Add new fields to pages:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $fields);

// Make fields visible in the TCEforms:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
  'fe_users', 
  '--div--;HiOrg-Anbindung,tx_rkmuc_hiorg_uid'
);
