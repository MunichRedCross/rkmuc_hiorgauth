<?php
if (!defined('TYPO3_MODE')) {
  die ('Access denied.');
}
  
// Configure new fields:
$fields = array(
  'tx_rkmuc_hiorg_ov' => array(
    'label' => 'Organisationskuerzel',
    'exclude' => 1,
    'config' => array(
      'type' => 'input',
      'max' => 6,
    ),
  ),
  'tx_rkmuc_addgroups' => array(
    'label' => 'Automatische Gruppenzuordnungen',
    'exclude' => 1,
    'config' => array(
      'type' => 'select',
      'autoSizeMax' => 10,
      'enableMultiSelectFilterTextfield' => 1,
      'foreign_table' => 'fe_groups',
      'foreign_table_where' => 'AND NOT(fe_groups.uid = ###THIS_UID###) AND fe_groups.hidden=0 ORDER BY fe_groups.title',
      'maxitems' => 20,
      'minitems' => 0,
      'renderType' => 'selectMultipleSideBySide',
      'size' => 6,
    ),
  ),
  'tx_rkmuc_parent_id' => array(
    'label' => 'Parent ID',
    'exclude' => 1,
    'config' => array(
      'type' => 'input',
      'readOnly' => 1,
      'max' => 3,
    ),
  ),
  'tx_rkmuc_hiorg_sid' => array(
    'label' => 'ID der HiOrg-Gruppe',
    'exclude' => 1,
    'config' => array(
      'type' => 'input',
      'readOnly' => 1,
      'max' => 4,
    ),
  ),
);
  
// Add new fields to pages:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_groups', $fields);
  
// Make fields visible in the TCEforms:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
  'fe_groups', 
  '--div--;HiOrg-Anbindung,tx_rkmuc_hiorg_ov, tx_rkmuc_hiorg_sid, tx_rkmuc_parent_id, tx_rkmuc_addgroups'
);
