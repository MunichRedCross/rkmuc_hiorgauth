<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function($extKey)
	{

        // add auth service
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addService(
            $_EXTKEY,
            'auth',
            \Rkmuc\Hiorgauth\Service\AuthenticationService::class,
            array(
                'title' => 'Authentication service for HiOrg',
                'description' => 'Authentication service for HiOrg.',
                'subtype' => 'processLoginDataFE, getUserFE, authUserFE, getGroupsFE',

                'available' => true,
                'priority' => 90,
                'quality' => 90,

                'os' => '',
                'exec' => '',

                'className' => \Rkmuc\Hiorgauth\Service\AuthenticationService::class,
            )
        );
        $GLOBALS['TYPO3_CONF_VARS']['SVCONF']['auth']['setup']['FE_fetchUserIfNoSession'] = true;

	// Typoscript
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($extKey, 'Configuration/TypoScript', 'Authmodule for HiORG');

	// Frontend Login Hooks
        $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['felogin']['postProcContent']['hiorg_marker'] = 'Rkmuc\Hiorgauth\Hooks\FeUserHook->postProcContent';
        $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['felogin']['login_confirmed']['hiorg_marker'] = 'Rkmuc\Hiorgauth\Hooks\FeUserHook->loginConfirmed';
        $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['felogin']['logout_confirmed']['hiorg_marker'] = 'Rkmuc\Hiorgauth\Hooks\FeUserHook->logoutConfirmed';


    },
    $_EXTKEY
);
